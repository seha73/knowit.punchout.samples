﻿using Knowit.PunchOut.Core.External;
using System;
using Knowit.PunchOut.Core.Business;
using EPiServer.Security;

namespace Knowit.Punchout.Samples.Implementations
{
    public class UserContextService : IUserContextService
    {


        public IUserContext Get()
        {
            var customerContext = new Mediachase.Commerce.Customers.CustomerContext();
            var contact = customerContext.GetContactByUserId(string.Format("String:{0}", PrincipalInfo.Current.Name));

            return new UserContext
            {
                OrganizationId = contact.ContactOrganization.PrimaryKeyId.ToString(),
                UserId = PrincipalInfo.Current.Name
            };
        }

        public IUserContext Get(string userId)
        {
            var customerContext = new Mediachase.Commerce.Customers.CustomerContext();
            var contact = customerContext.GetContactByUserId(string.Format("String:{0}",userId));

            return new UserContext
            {
                OrganizationId = contact.ContactOrganization.PrimaryKeyId.ToString(),
                UserId = userId
            };
        }

        private class UserContext : IUserContext
        {
            public string UserId { get; set; }
            public string OrganizationId { get; set; }
        }
    }
}