﻿using Knowit.PunchOut.Core;
using Knowit.PunchOut.Core.Business;
using System.Collections.Generic;

namespace EPiServer.Reference.Commerce.Site.Implementations
{
    // You might not be able to resolve an organization id or customer number to a data object, in that case you could mock an IOrganization object using the input data.
    public class MockOrganizationService : IOrganizationService
    {
        public IOrganization GetOrganization(string id)
        {
            return new OrganizationImpl() { Id = id, Name = id };
        }

        public IReadOnlyCollection<IOrganization> GetOrganizations(IEnumerable<string> organizationIds)
        {
            var result = new List<OrganizationImpl>();
            foreach (var id in organizationIds)
            {
                result.Add(new OrganizationImpl() { Id = id, Name = id });
            }
            return result;
        }
    }

    internal class OrganizationImpl : IOrganization
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

}