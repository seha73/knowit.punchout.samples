﻿using System.Linq;
using HandlebarsDotNet;

namespace Knowit.PunchOut.Core.Templating
{
    class FormatHelper : IHandlebarsHelper
    {
        public string Name => "Formatter";

        public string Prefix => "format";

        public string Definition => "Format a numerical variable by a pattern. Usage: format @variable \"0.00000\"";

        public HandlebarsHelper Execute()
        {
            return(writer, context, parameters) =>
            {
                var inputValue = parameters.FirstOrDefault();
                if (inputValue == null) return;

                var formatString = parameters.Skip(1).FirstOrDefault() as string;
                if (formatString == null) return;

                var formattedValue = GetFormattedValue(inputValue, formatString);
                if (formattedValue == null) return;

                writer.WriteSafeString(formattedValue);
            };
        }

        private static string GetFormattedValue(object inputValue, string formatString)
        {
            var defaultCulture = System.Globalization.CultureInfo.InvariantCulture;
            string formattedValue = null;
            try
            {
                if (inputValue is decimal)
                {
                    formattedValue = ((decimal)inputValue).ToString(formatString, defaultCulture);
                }
                else if (inputValue is double)
                {
                    formattedValue = ((double)inputValue).ToString(formatString, defaultCulture);
                }
                else if (inputValue is float)
                {
                    formattedValue = ((float)inputValue).ToString(formatString, defaultCulture);
                }
                else if (inputValue is int)
                {
                    formattedValue = ((int)inputValue).ToString(formatString, defaultCulture);
                }
            }
            catch
            {
            }

            return formattedValue;
        }
    }
}
